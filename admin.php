<?php
/**
 * Created by PhpStorm.
 * User: Vinkel
 * Date: 1/28/2018
 * Time: 4:37 AM
 */
?>

<?php require_once "common/header.php"; ?>

<div class="container" style="padding-top: 0px; padding-bottom: 110px;">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <section id="about" class="container waypoint">
                <div class="inner">

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Admin Panel | </span>Spare Parts</h1>
            <img src="images/icon-accmod.png" alt="icon">

                    </div>
                </section>

            <?php $sql = "SELECT tblparts.*,tblparts.productname,tblparts.description,tblparts.description,tblparts.pimage1,pimage2,pimage3,pimage4,price,discount  as bid  from tblparts";
            $query = $dbh->prepare($sql);
            $query->execute();
            $results = $query->fetchAll(PDO::FETCH_OBJ);
            $cnt = 1;
            if ($query->rowCount() > 0) {
            ?>

            <div class="container waypoint">
                <div class="inner">
                <h2 class="header light black fancy">Spare Part Products</h2>
                    <br />
                <p><i class="fa fa-product-hunt" aria-hidden="true"></i>
                    View of the listed Products:</p>
                    <div class="table-responsive">
                    <table class="table" style="text-align: left;">
                        <thead class="black">
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Discounted Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($results as $result) { ?>
                        <tr>
                            <td><?php echo htmlentities($result->id); ?></td>
                            <td><?php echo htmlentities($result->productname); ?></td>
                            <td><?php echo htmlentities($result->description); ?></td>
                            <td><i class="fa fa-dollar"></i> <?php echo htmlentities($result->price); ?></td>
                            <td><?php echo htmlentities($result->discount); ?> <i class="fa fa-percent"></i></td>
                            <?php
                            $price = $result->price;
                            $discount =$result->discount/100;
                            $discountedprice = $price*(1-$discount);
                            ?>
                            <td><i class="fa fa-dollar"></i> <?php echo $discountedprice; ?></td>
                            <td><a href="insert.php" class="btn btn-sm btn-info">Add <i class="fa fa-plus-circle"></i></a></td>
                            <td><a href="#" class="btn btn-sm btn-primary">Edit <i class="fa fa-pencil"></i></a></td>
                            <td><a href="#" class="btn btn-sm btn-danger">Delete <i class="fa fa-close"></i></a></td>
                        </tr>
                        <?php }
                        } ?>
                        </tbody>
                    </table>
                        </div>
                </div>
            </div>

            <?php $sql = "SELECT tblusers.*,tblusers.id,tblusers.FullName,tblusers.EmailId,tblusers.userrole,tblusers.ContactNo,tblusers.RegDate  as bid  from tblusers";
            $query = $dbh->prepare($sql);
            $query->execute();
            $results = $query->fetchAll(PDO::FETCH_OBJ);
            $cnt = 1;
            if ($query->rowCount() > 0) {
            ?>

            <div class="container waypoint">
                <div class="inner">
                    <h2 class="header light black fancy">List Of All Users And Their Current Roles</h2>
                    <br />
                    <p><i class="fa fa-users" aria-hidden="true"></i>
                        View Of All Registered Users:</p>
                    <div class="table-responsive">
                        <table class="table" style="text-align: left;">
                            <thead class="black">
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Current User Role</th>
                                <th>Contact</th>
                                <th>Registration Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($results as $result) { ?>
                                <tr>
                                    <td><?php echo htmlentities($result->id); ?></td>
                                    <td><?php echo htmlentities($result->FullName); ?></td>
                                    <td><?php echo htmlentities($result->EmailId); ?></td>
                                    <td>
                                        <?php
                                        if ($result->userrole == 1){
                                            $role = "Admin";
                                        } elseif($result->userrole == 2){
                                            $role = "Sales";
                                        } else {
                                            $role = "Customer";
                                        }
                                        ?>
                                        <?php echo $role; ?>
                                    </td>
                                    <td><?php echo htmlentities($result->ContactNo); ?></td>
                                    <td><?php echo htmlentities($result->RegDate); ?></td>
                                </tr>
                            <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



            <?php
            if (isset($_POST['submit'])) {
                $FullName = $_POST['FullName'];
                $assign = $_POST['assign'];
                $sql = "update tblusers set userrole=:assign where FullName=:FullName";
                $query = $dbh->prepare($sql);
                $query->bindParam(':FullName', $FullName, PDO::PARAM_STR);
                $query->bindParam(':assign', $assign, PDO::PARAM_STR);
                $query->execute();
                $msg = "User Role Updated Successfully";
            } else {
                $error = "Sorry, try Again";
            }
            ?>

            <div class="container waypoint">
                <div class="inner">
                    <?php
                    if ($msg) {
                        ?>
                        <div class="succWrap alert alert-success" role="alert"><strong>SUCCESS</strong>:<?php echo htmlentities($msg); ?>
                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        </div><?php } ?><br>
                    <h2 class="header light black fancy">Assign User Roles</h2>
                    <br />
                    <p><i class="fa fa-user-plus" aria-hidden="true"></i>
                        List Of All Registered Users: </p>
                    <div class="table-responsive">
                        <table class="table" style="text-align: left;">
                            <thead class="black">
                            <tr>
                                <th>Full Name</th>
                                <th>Assign Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <form method="post" enctype="multipart/form-data">

                                <td>
                                    <select class="form" name="FullName" required>
                                        <option value=""> Select User To Assign Role</option>
                                        <?php $ret = "select FullName from tblusers";
                                        $query = $dbh->prepare($ret);
                                        $query->execute();
                                        $results = $query->fetchAll(PDO::FETCH_OBJ);
                                        if ($query->rowCount() > 0) {
                                            foreach ($results as $result) {
                                                ?>
                                                <option
                                                    value="<?php echo htmlentities($result->FullName); ?>"><?php echo htmlentities($result->FullName); ?></option>
                                            <?php }
                                        } ?>

                                    </select>
                                </td>

                                <td>
                                        <div class="form-group">
                                            <input type="text" class="form-control form" name="assign" placeholder="User Role e.g. 1=Admin, 2=Sales, 3=Customer"
                                                   required="required">
                                        </div>

                                </td>
                                <td><button type="submit" name="submit" class="btn btn-lg btn-success">Assign Role <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    </button></td>
                                </form>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php require_once "common/footer.php"; ?>
