<?php
/**
 * Created by PhpStorm.
 * User: Vinkel
 * Date: 1/27/2018
 * Time: 11:59 PM
 */
?>

<?php require_once "common/header.php"; ?>

<!-- Products -->
<section id="about" class="container waypoint">
    <div class="inner">

        <!-- Header -->
        <h1 class="header light gray3 fancy"><span class="colored">Buy </span>Spare Parts</h1>
        <img src="images/icon-accmod.png" alt="icon">

        <!-- Description -->
        <p class="h-desc gray4">We have a<span class="colored bold"> wide variety</span> of Automobile Spare Parts at your service.<br /><br /></p>
        <hr>

        <!-- Alloy Wheels Boxes -->
        <div class="boxes" id="alloywheels">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Alloy </span>Wheels</h1>
            <hr><br>

<!--/*?php $sql = "SELECT tblparts.productname,tblparts.description,tblparts.pimage1,
            tblparts.pimage2,tblparts.pimage3,tblparts.pimage4,tblparts.price,
            tblparts.discount from tblparts ORDER BY id LIMIT 4";
$query = $dbh->prepare($sql);
$query->execute();
$results = $query->fetchAll(PDO::FETCH_OBJ);
$cnt = 1;
if ($query->rowCount() > 0) {
    foreach ($results as $result) {
        ?*/-->

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/g5.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Alloy Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Alloy Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 30
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->



                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/t2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Alloy Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Alloy Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 30
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/aw3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Alloy Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Alloy Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 30
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/aw4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Alloy Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Alloy Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 30
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Wheel Spanner Boxes -->
        <div class="boxes" id="wheelspannels">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Wheel </span>Spanners</h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/ws1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Wheel Spanners</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Wheel Spanners at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 15
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/ws2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Wheel Spanners</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Wheel Spanners at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 15
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/ws3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Wheel Spanners</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Wheel Spanners at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 15
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/ws4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Wheel Spanners</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Wheel Spanners at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 15
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Steering Wheels Boxes -->
        <div class="boxes" id="steeringwheels">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Steering </span>Wheels</h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sw1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Steering Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Steering Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 10
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sw2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Steering Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Steering Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 10
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sw3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Steering Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Steering Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 10
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sw4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Steering Wheels</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Steering Wheels at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 10
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Brake Pads Boxes -->
        <div class="boxes" id="brakepads">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Break </span>Pads</h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/bp1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Break Pads</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Brake Pads at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/bp2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Break Pads</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Break Pads at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/bp3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Break Pads</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Break Pads at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/bp4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Break Pads</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Break Pads at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Tyres Boxes -->
        <div class="boxes" id="tyres">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Tyres</span></h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/t1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Tyres</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Tyres at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/t2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Tyres</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Tyres at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/t3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Tyres</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Tyres at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/t4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Tyres</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Tyres at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Mounting Point Boxes -->
        <div class="boxes" id="mountingpoints">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Mounting </span>Point</h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/mp1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Mounting Point</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Mounting Point at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/mp2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Mounting Point</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Mounting Point at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/mp3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Mounting Point</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Mounting Point at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/mp4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Mounting Point</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Mounting Point at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 200
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Side Mirrors Boxes -->
        <div class="boxes" id="sidemirrors">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Side </span>Mirrors</h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sm1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Side Mirrors</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Side Mirrors at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 40
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sm2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Side Mirrors</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Side Mirrors at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 40
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sm3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Side Mirrors</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Side Mirrors at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 40
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/sm4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Side Mirrors</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Side Mirrors at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 40
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

        <!-- Head Lights Boxes -->
        <div class="boxes" id="headlights">
            <br />

            <!-- Header -->
            <h1 class="header light gray3 fancy"><span class="colored">Head </span>Lights</h1>
            <hr><br>

            <div class="team-members inner-details">
                <!-- Product 1 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="0">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/hl1.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Head Lights</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Head Lights at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 2 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/hl2.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Head Lights</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Head Lights at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 3 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="200">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/hl3.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Head Lights</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Head Lights at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Link -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->


                <!-- Product 4 -->
                <div class="col-xs-3 member animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="member-inner">
                        <!-- Team Member Image -->
                        <a class="team-image">
                            <!-- Img -->
                            <img class="img-responsive" src="images/download/hl4.jpg" alt="" />
                        </a>
                        <div class="member-details">
                            <div class="member-details-inner">
                                <!-- Name -->
                                <h2 class="member-name light">Head Lights</h2>
                                <!-- Description -->
                                <p class="member-description">High Quality Head Lights at Affordable Price.</p>
                                <!-- Socials -->
                                <div class="socials">
                                    <!-- Image -->
                                    <a href="singlepage.php"><i class="fa fa-link"></i></a>
                                </div><!-- End Socials -->
                                <div class="clear"></div>
                                <div class="text-center black"><!-- Price of Item -->
                                    <label class="control-label">Price: </label>
                                    <i class="fa fa-dollar"></i> 50
                                </div><!-- End of Price of item -->
                                <br />
                                <div class="clear"></div>
                                <div class="btn btn-primary">Buy</div>
                            </div> <!-- End Detail Inner -->
                        </div><!-- End Details -->
                    </div> <!-- End Member Inner -->
                </div><!-- End Member -->
            </div>
        </div><!-- End of boxes -->

    </div><!-- End About Inner -->
</section><!-- End Products Section -->
<!-- Top Button -->
<a href="#home" class="scroll top-button pull-right">
    <i class="fa fa-arrow-circle-up fa-2x"></i>
</a>

<?php require_once "common/footer.php"; ?>