<!--
Spare parts website
-->
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Product Full Description </title>
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
		<link href="pages/css/style.css" rel="stylesheet" type="text/css"  media="all" />
		<link href='//fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css'>
		<script src="pages/js/jquery.min.js"></script>
		<style type="text/css">
		
		</style>
	</head>
	<body style="background-color: #bdbdbd;">
			<?php require_once "common/header.php"; ?>
			<!---start-wrap--->
			<div class="wrap container" style="margin-bottom: 20px; padding-top: 25px; background-color: #bdbdbd;">
				<div class="content row">
					<?php $sql = "SELECT tblparts.*,tblparts.productname,tblparts.description,tblparts.description,tblparts.pimage1,pimage2,pimage3,pimage4,price,discount  as bid  from tblparts";
					$query = $dbh->prepare($sql);
					$query->execute();
					$results = $query->fetchAll(PDO::FETCH_OBJ);
					$cnt = 1;
					if ($query->rowCount() > 0) {
					?>

					<?php foreach ($results as $result) { ?>
					<div class="single-page"><br />
						<div class="single-top-pagination container header light gray1 animated">
							<ul>
								<li><a href="index.php">Home /</a></li>
								<li><span>Product Description</span></li>
							</ul>
						</div>
						<div class="clear"> </div>
						<div class="product-info container">
							<div class="product-image">
								<div class="flexslider">
							        <!-- FlexSlider -->
										<script src="pages/js/imagezoom.js"></script>
										  <script defer src="pages/js/jquery.flexslider.js"></script>
										<link rel="stylesheet" href="pages/css/flexslider.css" type="text/css" media="screen" />

										<script>
										// Can also be used with $(document).ready()
										$(window).load(function() {
										  $('.flexslider').flexslider({
											animation: "slide",
											controlNav: "thumbnails"
										  });
										});
										</script>
									<!-- //FlexSlider-->

							  <ul class="slides">
								<li data-thumb="assets/images/productimages/<?php echo htmlentities($result->pimage1); ?>">
									<div class="thumb-image"> <img src="assets/images/productimages/<?php echo htmlentities($result->pimage1); ?>" data-imagezoom="true" class="img-responsive"> </div>
								</li>
								<li data-thumb="assets/images/productimages/<?php echo htmlentities($result->pimage2); ?>">
									 <div class="thumb-image"> <img src="assets/images/productimages/<?php echo htmlentities($result->pimage2); ?>" data-imagezoom="true" class="img-responsive"> </div>
								</li>
								<li data-thumb="assets/images/productimages/<?php echo htmlentities($result->pimage3); ?>">
								   <div class="thumb-image"> <img src="assets/images/productimages/<?php echo htmlentities($result->pimage3); ?>" data-imagezoom="true" class="img-responsive"> </div>
								</li>
								<li data-thumb="assets/images/productimages/<?php echo htmlentities($result->pimage4); ?>">
								   <div class="thumb-image"> <img src="assets/images/productimages/<?php echo htmlentities($result->pimage4); ?>" data-imagezoom="true" class="img-responsive"> </div>
								</li>
							  </ul>
							<div class="clear"></div>
						</div>
							</div>
							<div class="product-price-info light black animated">
								<div class="product-catrgory-pagenation">
									<ul>
										<li><h3>Spare Part:</h3></li>
										<li class="active3"><a href="#"><?php echo htmlentities($result->productname); ?></a></li>
									</ul>
								</div>
									<div class="product-value lead">
									<h4>Product-Complete Details With Value</h4>
									<ul>
										<?php
										$price = $result->price;
										$discount =$result->discount/100;
										$discountedprice = $price*(1-$discount);
										?>
										<li><h2>Price :</h2></li>
										<li><span><?php echo htmlentities($result->price); ?></span></li>
										<li><h5><?php echo $discountedprice; ?></h5></li>
										<li><a href="#">Instock</a></li>
									</ul>
									<ul>
										<li><h3>Discount: <?php echo htmlentities($result->discount); ?> <i class="fa fa-percent"></i></h3></li>
										<li><h5>No reviews</h5></li>
									</ul>
								</div>
								<div class="product-shipping colored">
									<span>Shipping :</span>
									<p><lable>FREE</lable> - Flat Rate Courier - Delivery anywhere in </p>
									<div class="clear"> </div>
								</div>
								<div class="product-payments">
									<span>Payments: :</span>
									<p><lable>paypal</lable> - (Credit card, EMI, Debit card, Online Bank Transfer), PaisaPay COD  </p>
									<div class="clear"> </div>
								</div>
								<div class="product-description">
									<h3>Description :</h3>
									<p><?php echo htmlentities($result->description); ?></p>
									<div class="clear"></div>
									<div class="btn btn-lg btn-primary">Buy <i class="fa fa-cart-arrow-down"></i> </div>
								</div>
								<div class="product-share">
									<h3>share on:</h3>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-rss"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="clear"> </div>
						</div>
					</div>
			</div>
				<?php }
				} ?>
			<div class="clear"> </div>
			</div>
			<!---End-wrap--->
			<div class="container"></div>
			<div class="container"></div>
			<br /><br /><br /><br /><br /><br />
			<?php require_once "common/footer.php"; ?>
	</body>
</html>

