<?php
session_start();
include('common/config.php');
error_reporting(0);

?>
<?php require_once "common/header.php"; ?>

<!-- Home Section -->
<section id="home" class="relative">
	<div id="slides">
		<div class="slides-container relative">
			<!-- Slider Images -->
			<div class="image2"></div>
			<div class="image1"></div>
			<div class="image3"></div>
			<div class="image4"></div>
			<div class="image5"></div>
			<!-- End Slider Images -->
		</div>
		<!-- Slider Controls -->
		<nav class="slides-navigation">
			<a href="#" class="next"></a>
			<a href="#" class="prev"></a>
		</nav>
	</div><!-- End Home Slides -->
	<div class="v2 absolute">
		<!-- Auto Typocraphic Texts -->
		<div class="typographic">
			<!-- Your Logo -->
			<div class="logo">
				<img src="images/logo1.jpg" width="200" alt="Logo" />
			</div>
			<h2 class=" condensed uppercase no-padding no-margin bold gray1">Introducing</h2>
			<h2 class=" condensed uppercase no-padding no-margin bold colored">Quality Spare Parts</h2>
			<a href="#features" class="scroll"><i class="arrow-down fa fa-3x fa-angle-double-down"></i></a>
		</div><!--End Auto Typocraphic Texts -->
	</div><!-- End V2 area -->
</section><!-- End Home Section -->

<!-- Blockquote -->
<section id="blockquote">

	<div class="inner no-padding">
		<!-- Your Text -->
		<p class="text-center"><h1 class="header light gray1 animated" data-animation="fadeInLeft" data-animation-delay="400">FEATURED SPARE PART PRODUCTS</h1>
			</p>
	</div>

</section><!-- End Blockquote Section -->

<!-- Clients -->
<section id="clients" class="container">

	<!-- Team Inner -->
	<div class="inner team">

		<!-- Header -->
		<h1 class="header light gray3 fancy"><span class="colored">Our </span> Spare Part Products</h1>

		<!-- Description -->
		<p class="h-desc gray4">We offer<span class="colored bold">  The Best</span> Automobile Spare Parts in the country.</p>


		<!-- Products -->
		<div class="team-members inner-details">

			<!-- Product 1 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="0">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/b1.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">BUMPER</h2>
							<!-- Description -->
							<p class="member-description">High Quality Bumper at Affordable Price.</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Link -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 200
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->


			<!-- Product 2 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="100">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/g6.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Control Screen</h2>
							<!-- Description -->
							<p class="member-description">High Quality Vehicle Control Interface.</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Image -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 150
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->


			<!-- Product 3 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="200">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/h4.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Head Light</h2>
							<!-- Description -->
							<p class="member-description">High Quality Head Lights at Affordable Costs.</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Link -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 50
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->


			<!-- Product 4 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="300">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/t4.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">TYRES</h2>
							<!-- Description -->
							<p class="member-description">High Quality Yana Tyres at Affordable Costs.</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Image -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 200
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->


			<!-- Product 5 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="400">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/g4.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Steering Wheel</h2>
							<!-- Description -->
							<p class="member-description">High Quality UK Steering Wheels at Affordable Costs..</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Link -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 10
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->

			<!-- Product 6 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="0">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/h5.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Visibility Lights</h2>
							<!-- Description -->
							<p class="member-description">High Quality Visibility Lights at Affordable Costs..</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Link -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 50
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->


			<!-- Product 7 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="100">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/t2.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Alloy Wheels</h2>
							<!-- Description -->
							<p class="member-description">Advanced High Quality Alloy Wheels at Affordable Costs.</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Image -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 30
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->


			<!-- Product 8 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="200">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/g8.png" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Car Engine</h2>
							<!-- Description -->
							<p class="member-description">Durable Car Engine at Affordable Costs..</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Link -->
								<a href="singlepage.php"><i class="fa fa-link"></i></a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 500
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->

			<!-- Product 9 -->
			<div class="col-xs-4 member animated" data-animation="fadeInUp" data-animation-delay="500">
				<div class="member-inner">
					<!-- Team Member Image -->
					<a class="team-image">
						<!-- Img -->
						<img src="images/t3.jpg" alt="" />
					</a>
					<div class="member-details">
						<div class="member-details-inner">
							<!-- Name -->
							<h2 class="member-name light">Break Pads</h2>
							<!-- Description -->
							<p class="member-description">High Quality greased Break Pads at Affordable Costs..</p>
							<!-- Socials -->
							<div class="socials">
								<!-- Image -->
								<a href="singlepage.php"><i class="fa fa-link"></i> View</a>
							</div><!-- End Socials -->
							<div class="clear"></div>
							<div class="text-center black"><!-- Price of Item -->
								<label class="control-label">Price: </label>
								<i class="fa fa-dollar"></i> 50
							</div><!-- End of Price of item -->
							<br />
							<div class="clear"></div>
							<div class="btn btn-primary">Buy</div>
						</div> <!-- End Detail Inner -->
					</div><!-- End Details -->
				</div> <!-- End Member Inner -->
			</div><!-- End Member -->
		</div><!-- End Members -->
	</div><!-- End Team Inner -->
</section><!-- End Team Section -->

<!-- About Us -->
<section id="features" class="container">

	<div class="inner">

		<!-- Header -->
		<h1 class="header light gray1">ABOUT <span class="colored fancy">US!</span></h1>

		<!-- Description -->
		<p class="h-desc white bold">We are a <span class="colored bold"> leading </span> automobile spare parts company with all the resources you need.</p>

		<div class="features-boxes">

			<!-- Box 1 -->
			<div class="col-xs-4 f-box animated" data-animation="fadeIn" data-animation-delay="0">
				<!-- Icon -->
				<a class="f-icon">
					<i class="fa fa-car"></i>
				</a>
				<!-- Header -->
				<p class="feature-head">Car Parts Shop</p>
				<!-- Text -->
				<p class="feature-text ">Buy carparts is one of the leading car parts online shops due to a long-term experience and thousands of satisfied customers. Make yourself sure with our service as well as excellent price offers.</p>
			</div>


			<!-- Box 2 -->
			<div class="col-xs-4 f-box animated" data-animation="fadeIn" data-animation-delay="100">
				<!-- Icon -->
				<a class="f-icon">
					<i class="fa fa-tablet"></i>
				</a>
				<!-- Header -->
				<p class="feature-head">Original Parts Quality</p>
				<!-- Text -->
				<p class="feature-text ">Certified and reliable suppliers provide the entire range of auto parts. By Buycarparts.co.uk you get top brand spare parts. Competent staff of our support team will always help you in your search.</p>
			</div>


			<!-- Box 3 -->
			<div class="col-xs-4 f-box animated" data-animation="fadeIn" data-animation-delay="200">
				<!-- Icon -->
				<a class="f-icon">
					<i class="fa fa-truck"></i>
				</a>
				<!-- Header -->
				<p class="feature-head">High quality brand Auto parts</p>
				<!-- Text -->
				<p class="feature-text light">A large variety of manufacturers and high quality auto parts guarantee mobility of your vehicle and safety on roads. Count on branded car spare and Buycarparts.co.uk as a reliable auto parts supplier.</p>
			</div>
			<div class="clear"></div>

		</div><!-- End Features Boxes -->

	</div> <!-- End Features Inner -->

</section><!-- End Features Section -->

<!-- Testimonials -->
<section id="testimonial" class="testimonials parallax2">

	<div class="inner">

		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<ul>
						<li class="monial">
							<!-- Text -->
							<h1 class="condensed colored">"<span class="colored">The Spare Parts</span> we bought from you are really durable and long-lasting."</h1>
							<!-- Name -->
							<p class="light">John Doe</p>
						</li>
					</ul>
				</div>
				<div class="item">
					<ul>
						<li class="monial">
							<!-- Text -->
							<h1 class="condensed colored"><span class="colored">We decided to</span> stick with you as our spare parts provider after getting counterfeit services from other dealers for so long."</h1>
							<!-- Name -->
							<p class="black">Jane Doe</p>
						</li>
					</ul>
				</div>
				<div class="item">
					<ul>
						<li class="monial">
							<!-- Text -->
							<h1 class="condensed colored">"<span class="colored">This is one company</span> with a self preserved quality advertisement. Every product from them is incredibly high quality."</h1>
							<!-- Name -->
							<p class="black">Severe Dane</p>
						</li>
					</ul>
				</div>
			</div>


		</div>

	</div><!-- End Inner Div -->

</section><!-- End Testimonials Section -->

<!-- Blockquote -->
<section id="blockquote">

	<div class="inner no-padding">
		<!-- Your Text -->
		<div class="col-sm-8">
			<p class="normal white blockquote fancy">The aim is our Client's motivation, we work with Passion! </div>
		<a href="#features" class="scroll"><i class="fa fa-arrow-right"></i></a></p>
	</div>

</section><!-- End Blockquote Section -->

<!-- Contact Section -->
<section id="contact" class="container parallax4">
	<!-- Contact Inner -->
	<div class="inner contact">

		<!-- Form Area -->
		<div class="contact-form">

			<h4 class="header light gray3 fancy"><span class="colored">Contact</span> Us</h4>
			<p class="h-desc white">You can reach us anytime.<br />
				Email us or give us a call at <span class="bold colored">+254 723 326 451.</span></p>
			<!-- Form -->
			<form id="contact-us" method="post" action="#">
				<!-- Left Inputs -->
				<div class="col-xs-6 animated" data-animation="fadeInLeft" data-animation-delay="300">
					<!-- Name -->
					<input type="text" name="name" id="name" required="required" class="form" placeholder="Name" />
					<!-- Email -->
					<input type="email" name="mail" id="mail" required="required" class="form" placeholder="Email" />
					<!-- Subject -->
					<input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
				</div><!-- End Left Inputs -->
				<!-- Right Inputs -->
				<div class="col-xs-6 animated" data-animation="fadeInRight" data-animation-delay="400">
					<!-- Message -->
					<textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
				</div><!-- End Right Inputs -->
				<!-- Bottom Submit -->
				<div class="relative fullwidth col-xs-12">
					<!-- Send Button -->
					<button type="submit" id="submit" name="submit" class="form-btn semibold">Send Message <i class="fa fa-send"></i></button>
				</div><!-- End Bottom Submit -->
				<!-- Clear -->
				<div class="clear"></div>
			</form>

			<!-- Your Mail Message -->
			<div class="mail-message-area">
				<!-- Message -->
				<div class="alert gray-bg mail-message not-visible-message">
					<strong>Thank You !</strong> Your email has been delivered.
				</div>
			</div>

		</div><!-- End Contact Form Area -->
	</div><!-- End Inner -->
</section><!-- End Contact Section -->



<!-- Site Socials and Address -->
<section id="site-socials" class="no-padding white-bg">
	<div class="site-socials inner no-padding">
		<!-- Socials -->
		<div class="socials animated" data-animation="fadeInLeft" data-animation-delay="400">
			<!-- Facebook -->
			<a href="#" target="_blank" class="social">
				<i class="fa fa-facebook"></i>
			</a>
			<!-- Twitter -->
			<a href="#" target="_blank" class="social">
				<i class="fa fa-twitter"></i>
			</a>
			<!-- Instagram -->
			<a href="#" class="social">
				<i class="fa fa-instagram"></i>
			</a>
			<!-- Linkedin -->
			<a href="#" target="_blank" class="social">
				<i class="fa fa-linkedin"></i>
			</a>
			<!-- Vimeo -->
			<a href="#" target="_blank" class="social">
				<i class="fa fa-vimeo-square"></i>
			</a>
			<!-- Youtube -->
			<a href="#" target="_blank" class="social">
				<i class="fa fa-youtube"></i>
			</a>
			<!-- Google Plus -->
			<a href="#" target="_blank" class="social">
				<i class="fa fa-google-plus"></i>
			</a>
		</div>
		<!-- Adress, Mail -->
		<div class="address socials animated" data-animation="fadeInRight" data-animation-delay="500">
			<!-- Phone Number, Mail -->
			<p>Phone: +254 723 326 451 Email : <a href="mailto:info@Fitness.com" class="colored">info@spareparts.com</a> Address: 23 Westlands, Nairobi, Kenya</p>
		</div><!-- End Adress, Mail -->
		<!-- Top Button -->
		<a href="#home" class="scroll top-button pull-right">
			<i class="fa fa-arrow-circle-up fa-2x"></i>
		</a>
	</div><!-- End Inner -->
</section><!-- End Site Socials and Address -->

<?php require_once "common/footer.php"; ?>


