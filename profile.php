<?php
session_start();
error_reporting(0);
include('common/config.php');
if (strlen($_SESSION['login']) == 0) {
    header('location:index.php');
} else {
    if (isset($_POST['updateprofile'])) {
        $name = $_POST['fullname'];
        $mobileno = $_POST['mobilenumber'];
        $dob = $_POST['dob'];
        $adress = $_POST['address'];
        $city = $_POST['city'];
        $country = $_POST['country'];
        $email = $_SESSION['login'];
        $sql = "update tblusers set FullName=:name,ContactNo=:mobileno,dob=:dob,Address=:adress,City=:city,Country=:country where EmailId=:email";
        $query = $dbh->prepare($sql);
        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->bindParam(':mobileno', $mobileno, PDO::PARAM_STR);
        $query->bindParam(':dob', $dob, PDO::PARAM_STR);
        $query->bindParam(':adress', $adress, PDO::PARAM_STR);
        $query->bindParam(':city', $city, PDO::PARAM_STR);
        $query->bindParam(':country', $country, PDO::PARAM_STR);
        $query->bindParam(':email', $email, PDO::PARAM_STR);
        $query->execute();
        $msg = "Profile Updated Successfully";
    }

    ?>
        <style>
            .errorWrap {
                padding: 10px;
                margin: 0 0 20px 0;
                background: #fff;
                border-left: 4px solid #dd3d36;
                -webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
                box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
            }

            .succWrap {
                padding: 10px;
                margin: 0 0 20px 0;
                background: #fff;
                border-left: 4px solid #5cb85c;
                -webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
                box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
            }
        </style>
    <body>

    <!--Header-->
    <?php require_once "common/header.php"; ?>
    <!-- /Header -->
    <!--Page Header-->
    <section class="page-header profile_page">
        <div class="container">
            <div class="page-header_wrap">
                <div class="page-heading">
                    <h1>Your Profile</h1>
                </div>
                <ul class="coustom-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>Profile</li>
                </ul>
            </div>
        </div>
        <!-- Dark Overlay-->
        <div class="dark-overlay"></div>
    </section>
    <!-- /Page Header-->


    <?php
    $useremail = $_SESSION['login'];
    $sql = "SELECT * from tblusers where EmailId=:useremail";
    $query = $dbh->prepare($sql);
    $query->bindParam(':useremail', $useremail, PDO::PARAM_STR);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_OBJ);
    $cnt = 1;
    if ($query->rowCount() > 0)
    {
    foreach ($results as $result)
    { ?>
    <section class="user_profile inner_pages">
        <div class="container">


            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="profile_wrap">
                        <h5 class="uppercase underline">General Settings</h5>
                        <?php
                        if ($msg) {
                            ?>
                            <div class="succWrap alert" role="alert"><strong>SUCCESS</strong>:<?php echo htmlentities($msg); ?>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            </div><?php } ?>
                        <form method="post">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Reg Date -</label>
                                    <?php echo htmlentities($result->RegDate); ?>
                                </div>
                                <?php if ($result->UpdationDate != "") { ?>
                                    <div class="form-group">
                                        <label class="control-label">Last Update at -</label>
                                        <?php echo htmlentities($result->UpdationDate); ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="control-label">Full Name</label>
                                    <input class="form-control white_bg" name="fullname"
                                           value="<?php echo htmlentities($result->FullName); ?>" id="fullname"
                                           type="text" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <input class="form-control white_bg"
                                           value="<?php echo htmlentities($result->EmailId); ?>" name="emailid"
                                           id="email" type="email" required readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <input class="form-control white_bg" name="mobilenumber"
                                           value="<?php echo htmlentities($result->ContactNo); ?>" id="phone-number"
                                           type="text" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Date of Birth&nbsp;(dd/mm/yyyy)</label>
                                    <input class="form-control white_bg"
                                           value="<?php echo htmlentities($result->dob); ?>" name="dob"
                                           placeholder="dd/mm/yyyy" id="birth-date" type="text">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Country</label>
                                    <input class="form-control white_bg" id="country" name="country"
                                           value="<?php echo htmlentities($result->City); ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <input class="form-control white_bg" id="city" name="city"
                                           value="<?php echo htmlentities($result->City); ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Your Address</label>
                                    <textarea class="form-control white_bg" name="address"
                                              rows="4"><?php echo htmlentities($result->Address); ?></textarea>
                                </div>

                            </div>


                            <?php }
                            } ?>

                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"><div class="form-group">
                                    <button type="submit" name="updateprofile" class="btn">Save Changes <span
                                            class="angle_arrow"><i class="fa fa-angle-right"
                                                                   aria-hidden="true"></i></span></button>
                                </div></div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    <!--/Profile-setting-->

    <<!--Footer -->
    <?php include('common/footer.php'); ?>
    <!-- /Footer-->

    <!--Back to top-->
    <div id="back-top" class="back-top"><a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a></div>
    <!--/Back to top-->

    <!--Login-Form -->
    <?php include('common/login.php'); ?>
    <!--/Login-Form -->

    <!--Register-Form -->
    <?php include('common/registration.php'); ?>

    <!--/Register-Form -->

    <!--Forgot-password-Form -->
    <?php include('common/forgotpassword.php'); ?>
    <!--/Forgot-password-Form -->

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/interface.js"></script>
    <!--Switcher-->
    <script src="assets/switcher/js/switcher.js"></script>
    <!--bootstrap-slider-JS-->
    <script src="assets/js/bootstrap-slider.min.js"></script>
    <!--Slider-JS-->
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>

    </body>
    </html>
<?php } ?>