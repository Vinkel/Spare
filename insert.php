<?php
/**
 * Created by PhpStorm.
 * User: Vinkel
 * Date: 1/28/2018
 * Time: 12:12 PM
 */
?>

<?php

session_start();
error_reporting(0);
include('common/config.php');
if (strlen($_SESSION['login']) == 0) {
    header('location:index.php');
} else {

if (isset($_POST['submit'])) {
    $productname = $_POST['productname'];
    $description = $_POST['description'];
    $pimage1 = $_FILES["img1"]["name"];
    $pimage2 = $_FILES["img2"]["name"];
    $pimage3 = $_FILES["img3"]["name"];
    $pimage4 = $_FILES["img4"]["name"];
    $price = $_POST['price'];
    $discount = $_POST['discount'];
    move_uploaded_file($_FILES["img1"]["tmp_name"], "assets/images/productimages/" . $_FILES["img1"]["name"]);
    move_uploaded_file($_FILES["img2"]["tmp_name"], "assets/images/productimages/" . $_FILES["img2"]["name"]);
    move_uploaded_file($_FILES["img3"]["tmp_name"], "assets/images/productimages/" . $_FILES["img3"]["name"]);
    move_uploaded_file($_FILES["img4"]["tmp_name"], "assets/images/productimages/" . $_FILES["img4"]["name"]);

    $sql = "INSERT INTO tblparts(productname, description,pimage1,pimage2,pimage3,pimage4, price,discount) 
VALUES(:productname, :description,:pimage1,:pimage2,:pimage3,:pimage4, :price,:discount)";
    $query = $dbh->prepare($sql);
    $query->bindParam(':productname', $productname, PDO::PARAM_STR);
    $query->bindParam(':description', $description, PDO::PARAM_STR);
    $query->bindParam(':pimage1', $pimage1, PDO::PARAM_STR);
    $query->bindParam(':pimage2', $pimage2, PDO::PARAM_STR);
    $query->bindParam(':pimage3', $pimage3, PDO::PARAM_STR);
    $query->bindParam(':pimage4', $pimage4, PDO::PARAM_STR);
    $query->bindParam(':price', $price, PDO::PARAM_STR);
    $query->bindParam(':discount', $discount, PDO::PARAM_STR);
    $query->execute();
    $lastInsertId = $dbh->lastInsertId();
    if ($lastInsertId) {
        $msg = "Spare Part Added successfully";
    } else {
        $error = "Something went wrong. Please try again";
    }

}

?>

<?php require_once "common/header.php"; ?>

<div class="ts-main-content" style="padding-top: 20px; padding-bottom: 70px;">
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-top: 50px;">
                <div class="col-md-12">
                    <h2 class="header light gray3 fancy" style="text-align: center;">Add Spare Part</h2>
                    <br />

                    <div class="row" style="padding-top: 20px;">
                        <div class="col-md-12">
                            <div class="panel panel-primary" style="padding-top: 20px;">
                                <div class="panel-heading header light gray3 fancy"><strong>Product Details</strong></div>
                                <?php if ($error) { ?>
                                    <div class="errorWrap alert alert-danger"><strong>ERROR</strong>
                                    :<?php echo htmlentities($msg); ?>
                                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a></div><?php } else if ($msg) { ?>
                                    <div class="succWrap alert alert-success"><strong>SUCCESS</strong>:<?php echo htmlentities($msg); ?>
                                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                    </div><?php } ?>

                                <div class="panel-body">
                                    <form method="post" class="form-horizontal" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Product Name<span
                                                    style="color:red">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="text" name="productname" class="form form-control" placeholder="Product Name"
                                                       required>
                                            </div>
                                        </div>

                                        <div class="hr-dashed"></div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Product Description<span
                                                    style="color:red">*</span></label>
                                            <div class="col-sm-4">
                                                    <textarea class="form form-control" name="description" rows="3" placeholder="Product Description"
                                                              required></textarea>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Price<span
                                                    style="color:red">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="text" name="price" class="form form-control" placeholder="Price e.g. 200" required>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Disount<span
                                                    style="color:red">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="text" name="discount" class="form form-control" placeholder="Discount e.g 5" required>
                                            </div>
                                        </div>
                                        <div class="hr-dashed"></div>


                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <h4 class="header light black fancy pull-left"><b>Upload Images</b></h4>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                Image 1 <span style="color:red">*</span><input type="file"
                                                                                               name="img1" required>
                                            </div>
                                            <div class="col-sm-4">
                                                Image 2<span style="color:red">*</span><input type="file"
                                                                                              name="img2" required>
                                            </div>
                                            <div class="col-sm-4">
                                                Image 3<span style="color:red">*</span><input type="file"
                                                                                              name="img3" required>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                Image 4<span style="color:red">*</span><input type="file"
                                                                                              name="img4" required>
                                            </div>

                                        </div>
                                        <div class="hr-dashed"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <button class="btn btn-lg btn-primary" name="submit" type="submit">Save
                         <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>

            </form>


        </div>
    </div>
</div>
<?php } ?>
<?php require_once "common/footer.php"; ?>