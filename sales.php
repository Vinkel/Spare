<?php
/**
 * Created by PhpStorm.
 * User: Vinkel
 * Date: 1/28/2018
 * Time: 4:38 AM
 */
?>

<?php require_once "common/header.php"; ?>

<div class="container" style="padding-top: 30px; padding-bottom: 110px;>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <section id="about" class="container waypoint">
                <div class="inner">

                    <!-- Header -->
                    <h1 class="header light gray3 fancy"><span class="colored">Sales Panel | </span>Spare Parts</h1>
                    <img src="images/icon-accmod.png" alt="icon">

                </div>
            </section>

            <?php $sql = "SELECT tblparts.*,tblparts.productname,tblparts.description,tblparts.description,tblparts.pimage1,pimage2,pimage3,pimage4,price,discount  as bid  from tblparts";
            $query = $dbh->prepare($sql);
            $query->execute();
            $results = $query->fetchAll(PDO::FETCH_OBJ);
            $cnt = 1;
            if ($query->rowCount() > 0) {
            ?>

            <div class="container waypoint">
                <div class="inner">
                    <h2 class="header light black fancy">All Spare Parts Products</h2>
                    <br />
                    <p>View of the listed Products:</p>
                    <div class="table-responsive">
                        <table class="table" style="text-align: left;">
                            <thead class="black">
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Discounted Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($results as $result) { ?>
                                <tr>
                                    <td><?php echo htmlentities($result->id); ?></td>
                                    <td><?php echo htmlentities($result->productname); ?></td>
                                    <td><?php echo htmlentities($result->description); ?></td>
                                    <td><i class="fa fa-dollar"></i> <?php echo htmlentities($result->price); ?></td>
                                    <?php
                                    $price = $result->price;
                                    $discount =$result->discount/100;
                                    $discountedprice = $price*(1-$discount);
                                    ?>
                                    <td><i class="fa fa-dollar"></i> <?php echo $discountedprice; ?></td>
                                    <td><a href="insert.php" class="btn btn-sm btn-info">Add <i class="fa fa-plus-circle"></i></a></td>
                                    <td><a href="#" class="btn btn-sm btn-primary">Edit <i class="fa fa-pencil"></i></a></td>
                                </tr>
                            <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php require_once "common/footer.php"; ?>
