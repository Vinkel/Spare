<?php
/**
 * Created by PhpStorm.
 * User: Vinkel
 * Date: 1/27/2018
 * Time: 2:03 PM
 */
?>

<!-- Footer -->
<footer id="footer" class="footer">
    <!-- Your Company Name -->
    <img src="images/logo.png" width="200" alt="Logo" />
    <!-- Copyright -->
    <p class="copyright normal">Copyright © 2018 <span class="colored">Spare Parts.</span> All Rights Reserved.</p>
</footer><!-- End Footer -->

<!--Login-Form -->
<?php include('common/login.php'); ?>
<!--/Login-Form -->

<!--Register-Form -->
<?php include('common/registration.php'); ?>

<!--/Register-Form -->

<!--Forgot-password-Form -->
<?php include('common/forgotpassword.php'); ?>
<!--/Forgot-password-Form -->

<!-- JS Files -->

<!--scripts and plugins -->
<!--must need plugin jquery-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate.min.js"></script>
<!--bootstrap js plugin-->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!--easing plugin for smooth scroll-->
<script src="js/jquery.easing.1.3.min.js" type="text/javascript"></script>
<!--sticky header-->
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<!--flex slider plugin-->
<script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
<!--parallax background plugin-->
<script src="js/jquery.stellar.min.js" type="text/javascript"></script>
<!--digit countdown plugin-->
<script src="js/waypoints.min.js"></script>
<!--digit countdown plugin-->
<script src="js/jquery.counterup.min.js" type="text/javascript"></script>
<!--on scroll animation-->
<script src="js/wow.min.js" type="text/javascript"></script>
<!--owl carousel slider-->
<script src="owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<!--popup js-->
<script src="js/jquery.magnific-popup.min.js" type="text/javascript"></script>


<!--customizable plugin edit according to your needs-->
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="js/revolution-custom.js"></script>
<!--cube portfolio plugin-->
<script src="cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="js/cube-portfolio.js" type="text/javascript"></script>
<script src="js/pace.min.js" type="text/javascript"></script>

<!-- Additional Fitness Styles -->
<script src="assets/js/jquery.appear.js" type="text/javascript"></script>
<script src="assets/js/jquery.fitvids.js" type="text/javascript"></script>
<script src="assets/js/jquery.flexslider.js" type="text/javascript"></script>
<script src="assets/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
<script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script src="assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="assets/js/jquery.slabtext.js" type="text/javascript"></script>
<script src="assets/js/jquery.superslides.js" type="text/javascript"></script>
<script src="assets/js/modernizr-latest.js" type="text/javascript"></script>
<script src="assets/js/plugins.js" type="text/javascript"></script>
<script src="assets/js/SmoothScroll.js" type="text/javascript"></script>
<!-- End of Additional Fitness Styles -->
</body>
</html>
