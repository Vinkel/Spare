<?php
/**
 * Created by PhpStorm.
 * User: Vinkel
 * Date: 1/27/2018
 * Time: 2:03 PM
 */
?>
<?php
session_start();
include('common/config.php');
error_reporting(0);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SPARE PARTS</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- font awesome for icons -->
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- flex slider css -->
	<link href="css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
	<!-- animated css  -->
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="screen">
	<!-- Revolution Style-sheet -->
	<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css">
	<link rel="stylesheet" type="text/css" href="css/rev-style.css">
	<!--owl carousel css-->
	<link href="owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" media="screen">
	<link href="owl-carousel/assets/owl.theme.default.css" rel="stylesheet" type="text/css" media="screen">
	<!--mega menu -->
	<link href="css/yamm.css" rel="stylesheet" type="text/css">
	<!--cube css-->
	<link href="cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css">
	<!-- custom css-->
	<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>

	<!-- Additional Fitness CSS Files -->
	<link href="assets/css/pro-bars.css" rel="stylesheet" type="text/css">
	<link href="assets/css/reset.css" rel="stylesheet" type="text/css">
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
	<link href="assets/css/style.css" rel="stylesheet" type="text/css">
	<!-- End of Additional Fitness CSS Files -->
	<!-- Start WOWSlider.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->

	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<div class="top-bar-dark">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-2 hidden-xs">
				<div class="top-bar-socials">
					<a href="#" class="social-icon-sm si-dark si-gray-round si-colored-facebook">
						<i class="fa fa-facebook"></i>
						<i class="fa fa-facebook"></i>
					</a>
					<a href="#" class="social-icon-sm si-dark si-gray-round si-colored-twitter">
						<i class="fa fa-twitter"></i>
						<i class="fa fa-twitter"></i>
					</a>
					<a href="#" class="social-icon-sm si-dark si-gray-round si-colored-google-plus">
						<i class="fa fa-google-plus"></i>
						<i class="fa fa-google-plus"></i>
					</a>
					<a href="#" class="social-icon-sm si-dark si-gray-round si-colored-linkedin">
						<i class="fa fa-linkedin"></i>
						<i class="fa fa-linkedin"></i>
					</a>
					<a href="#" class="social-icon-sm si-dark si-gray-round si-colored-google-plus">
						<i class="fa fa-youtube"></i>
						<i class="fa fa-youtube"></i>
					</a>
					<a href="#" class="social-icon-sm si-dark si-gray-round si-colored-dribbble">
						<i class="fa fa-dribbble"></i>
						<i class="fa fa-dribbble"></i>
					</a>
				</div>
			</div>
			<div class="col-sm-4 text-right">
				<ul class="list-inline top-dark-right">
					<li class="hidden-sm hidden-xs"><i class="fa fa-envelope"></i> info@spareparts.co.ke</li>
					<li class="hidden-sm hidden-xs"><i class="fa fa-phone"></i> +254 723 326 451</li>
					<?php if (strlen($_SESSION['login']) == 0) {
					?>
					<li><a href="#loginform" class="btn btn-xs uppercase" data-toggle="modal"
						   data-dismiss="modal"><i class="fa fa-lock"></i> Login</a></li>
					<li><a href="#signupform" data-toggle="modal" data-dismiss="modal"><i class="fa fa-user"></i> Sign Up</a></li>
					<?php } else {

						echo "Welcome To Spare Parts";
					} ?>
					<li><a class="topbar-icons" href="#"><span><i class="fa fa-search top-search"></i></span></a></li>
				</ul>
				<div class="search">
					<form role="form">
						<input type="text" class="form-control" autocomplete="off"
							   placeholder="Write something and press enter">
						<span class="search-close"><i class="fa fa-times"></i></span>
					</form>
				</div>

			</div>
		</div>
	</div>
</div><!--top-bar-dark end here-->

<!-- Navigation -->
<section id="navigation" class="dark-nav">
	<!-- Navigation Inner -->
	<div class="nav-inner">
		<!-- Site Logo -->
		<div class="site-logo fancy">
			<a href="index.php" id="logo-text" class="scroll logo">Spare Parts
			</a>
		</div><!-- End Site Logo -->
		<a class="mini-nav-button gray2"><i class="fa fa-bars"></i></a>
		<!-- Navigation Menu -->
		<div class="nav-menu">
			<ul class="nav uppercase">
				<li><a href="index.php" class="scroll"><i class="fa fa-home"></i> Home</a></li>
				<li><a href="index.php#features" class="scroll"><i class="fa fa-address-book-o"></i> About us</a></li>
				<li><a href="index.php#blockquote" class="scroll"><i class="fa fa-shopping-basket"></i> Featured</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle " data-toggle="dropdown" class="scroll"><i class="fa fa-shopping-bag"></i> Products <i
							class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu multi-level" role="menu">
						<li>
							<a tabindex="-1" href="parts.php#alloywheels">Alloywheels </a>
							<a tabindex="-1" href="parts.php#wheelspanners">Wheel Spanner </a>
							<a tabindex="-1" href="parts.php#steeringwheels">Steering Wheel </a>
							<a tabindex="-1" href="parts.php#breakpads">Break Pads </a>
							<a tabindex="-1" href="parts.php#tyres">Tyres </a>
							<a tabindex="-1" href="parts.php#mountingpoints">Mounting Points </a>
							<a tabindex="-1" href="parts.php#sidemirrors">Side Mirrors </a>
							<a tabindex="-1" href="parts.php#headlights">Head Lights </a>
						</li>
					</ul>
				</li>
				<li><a href="index.php#contact" class="scroll"><i class="fa fa-address-card-o"></i> Contact</a></li>
				<li><a href="#cart" class="scroll"><i class="fa fa-shopping-cart"></i> Cart</a></li>
				<li class="dropdown"><a href="#" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false"><i class="fa fa-user-circle"
																 aria-hidden="true"></i>
						<?php
						$email = $_SESSION['login'];
						$sql = "SELECT FullName, userrole FROM tblusers WHERE EmailId=:email";
						$query = $dbh->prepare($sql);
						$query->bindParam(':email', $email, PDO::PARAM_STR);
						$query->execute();
						$results = $query->fetchAll(PDO::FETCH_OBJ);
						if ($query->rowCount() > 0) {
							foreach ($results as $result) {

								echo htmlentities($result->FullName);
							}
						} ?><i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul class="dropdown-menu">
						<?php if ($_SESSION['login'] && $result->userrole == 1) { ?>
							<li><a href="#">Profile Settings</a></li>
							<li><a href="sales.php">Sales Dashboard</a></li>
							<br>
							<li><a href="admin.php">Administrator</a></li>
							<li><a href="logout.php">Sign Out</a></li>
						<?php } elseif ($_SESSION['login'] && $result->userrole == 2) { ?>
							<li><a href="#">Profile Settings</a></li>
							<li><a href="sales.php">Sales Dashboard</a></li>
							<br>
							<li><a href="logout.php">Sign Out</a></li>
						<?php } elseif ($_SESSION['login'] && $result->userrole == 3) { ?>
							<li><a href="#">Profile Settings</a></li>
							<li><a href="#">Update Password</a></li>
							<li><a href="parts.php#alloywheels">ALl Products</a>
							</li>
							<li><a href="logout.php">Sign Out</a></li>
						<?php } else { ?>
							<li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Profile
									Settings</a></li>
							<li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Update
									Password</a></li>
							<li><a href="#loginform" data-toggle="modal" data-dismiss="modal">ALl Products</a>
							</li>
							<li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Sign Out</a></li>
						<?php } ?>
						<?php { ?>
						<?php } ?>
					</ul>
				</li>
			</ul>
			<div class="user_login">
				<ul>

				</ul>
			</div>
		</div><!-- End Navigation Menu -->
	</div><!-- End Navigation Inner -->
</section><!-- End Navigation Section -->